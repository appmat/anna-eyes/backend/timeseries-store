FROM python:3.8.0

ADD . /flask-deploy
WORKDIR /flask-deploy

RUN pip install gunicorn[gevent]
RUN pip install -r r.txt
RUN pip install .

ENV PROMETHEUS_MULTIPROC_DIR /tmp
EXPOSE 5000

CMD gunicorn -c ./timeseries_store/wsgi/config.py --worker-class gevent --workers 2 --bind 0.0.0.0:5000 timeseries_store.wsgi:app --max-requests 10000 --timeout 5 --keep-alive 5 --log-level info
