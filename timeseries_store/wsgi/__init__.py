from timeseries_store import create_app
import os
FLASK_ENV = os.environ.get('FLASK_ENV', 'production')
app = create_app(FLASK_ENV)