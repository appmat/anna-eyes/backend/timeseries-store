"""Class definition for Point model."""
from datetime import datetime, timezone, timedelta
from email.policy import default
from pytz import utc
from sqlalchemy import BigInteger

from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.dialects.postgresql import UUID

from timeseries_store import db
from timeseries_store.util.datetime_util import (
    utc_now,
    get_local_utcoffset,
    make_tzaware,
    localized_dt_string,
)

class Point(db.Model):
    """Point model for a generic resource in a REST API."""

    __tablename__ = "point"

    id = db.Column(BigInteger, primary_key=True,autoincrement=True)
    timeseries_id = db.Column(BigInteger, db.ForeignKey("timeseries.id"), nullable=False)

    name = db.Column(db.String(32), nullable=False, default="")
    marked = db.Column(db.Boolean, nullable=False, default=False)
    value = db.Column(db.Float, nullable=False)
    index = db.Column(db.Integer, nullable=False)

    created_at = db.Column(db.DateTime, default=utc_now)
    updated_at = db.Column(db.DateTime, default=utc_now, onupdate=utc_now)

    __table_args__ = (
        db.Index(
            'point_unique_timeseries_marked_point_name',
            'timeseries_id', 'name',
            unique=True,
            postgresql_where=(marked.is_(True))),
        db.Index(
            'point_unique_timeseries_id_index',
            'timeseries_id', 'index',
            unique=True,
            ),
    )

    def __repr__(self):
        if self.marked:
            return f"<Point id={self.id}, name={self.name}, value={self.value}>"
        return f"<Point id={self.id}, value={self.value}>"

    @hybrid_property
    def created_at_str(self):
        created_at_utc = make_tzaware(
            self.created_at, use_tz=timezone.utc, localize=False
        )
        return localized_dt_string(created_at_utc, use_tz=get_local_utcoffset())

    @hybrid_property
    def updated_at_str(self):
        updated_at_utc = make_tzaware(
            self.updated_at, use_tz=timezone.utc, localize=False
        )
        return localized_dt_string(updated_at_utc, use_tz=get_local_utcoffset())
