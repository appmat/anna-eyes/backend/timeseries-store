"""Class definition for Timeseries model."""
from datetime import datetime, timezone, timedelta
from pytz import utc
import uuid
from sqlalchemy import BigInteger

from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.dialects.postgresql import UUID

from timeseries_store import db
from timeseries_store.util.datetime_util import (
    utc_now,
    get_local_utcoffset,
    make_tzaware,
    localized_dt_string,
)


class Timeseries(db.Model):
    """Timeseries model for a generic resource in a REST API."""

    __tablename__ = "timeseries"

    id = db.Column(BigInteger, primary_key=True,autoincrement=True)
    name = db.Column(db.String(512), nullable=False)
    created_at = db.Column(db.DateTime, default=utc_now)
    updated_at = db.Column(db.DateTime, default=utc_now, onupdate=utc_now)
    points = db.relationship("Point", backref=db.backref("timeseries"))

    def __repr__(self):
        return f"<Timeseries id={self.id}, name={self.name}>"

    @hybrid_property
    def created_at_str(self):
        created_at_utc = make_tzaware(
            self.created_at, use_tz=timezone.utc, localize=False
        )
        return localized_dt_string(created_at_utc, use_tz=get_local_utcoffset())

    @hybrid_property
    def updated_at_str(self):
        updated_at_utc = make_tzaware(
            self.updated_at, use_tz=timezone.utc, localize=False
        )
        return localized_dt_string(updated_at_utc, use_tz=get_local_utcoffset())
