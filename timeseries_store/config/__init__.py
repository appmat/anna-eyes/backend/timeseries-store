"""Config settings for for development, testing and production environments."""
import os
from pathlib import Path

HERE = Path(__file__).parent
POSTGRES_DEV = "postgresql://postgres:postgres@localhost:5432/app"
POSTGRES_TEST = "postgresql://postgres:postgres@localhost:5432/test"
POSTGRES_PROD = ""


class Config:
    """Base configuration."""

    # Секретный ключ для формирования JWT токена
    SECRET_KEY = os.getenv("SECRET_KEY", "open sesame")  
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    SWAGGER_UI_DOC_EXPANSION = "list"
    RESTX_MASK_SWAGGER = False
    JSON_SORT_KEYS = False

    OIDC_REQUIRE_VERIFIED_EMAIL = False
    OIDC_USER_INFO_ENABLED = True
    OIDC_SCOPES = ['openid', 'email', 'profile']
    OIDC_INTROSPECTION_AUTH_METHOD = 'client_secret_post'
    OIDC_ID_TOKEN_COOKIE_SECURE = False
    OIDC_OPENID_REALM = 'master'
    OIDC_TOKEN_TYPE_HINT = 'access_token'
    OIDC_CLIENT_SECRETS = 'client_secrets.json'

class TestingConfig(Config):
    """Testing configuration."""
    TESTING = True
    SQLALCHEMY_DATABASE_URI = POSTGRES_TEST


class DevelopmentConfig(Config):
    """Development configuration."""
    OIDC_CLIENT_SECRETS = './timeseries_store/config/client_secrets_dev.json'
    SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL", POSTGRES_DEV)

class ProductionConfig(Config):
    """Production configuration."""
    OIDC_CLIENT_SECRETS = './timeseries_store/config/client_secrets_prod.json'
    SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL", POSTGRES_PROD).replace("postgres://","postgresql://")
    PRESERVE_CONTEXT_ON_EXCEPTION = True

ENV_CONFIG_DICT = dict(
    development=DevelopmentConfig, testing=TestingConfig, production=ProductionConfig
)

def get_config(config_name):
	"""Retrieve environment configuration settings."""
	return ENV_CONFIG_DICT.get(config_name, ProductionConfig)