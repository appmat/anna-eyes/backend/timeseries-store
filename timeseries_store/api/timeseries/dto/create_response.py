from typing import List
from attr import field
from flask_restx import Model, fields

from timeseries_store.api.timeseries.endpoints import timeseries_ns

timeseries = timeseries_ns.model('TimeseriesResponse', {
    'id': fields.String,
    'name': fields.String,
})

create_timeseries_resp = timeseries_ns.model('CreateTimeseriesResponse', {
    'result': fields.List(fields.Nested(timeseries)),
})
