from typing import List
from attr import field
from flask_restx import Model, fields

from timeseries_store.api.timeseries.endpoints import timeseries_ns

timeseries = timeseries_ns.model('TimeseriesResponse', {
    'id': fields.String,
    'name': fields.String,
})

read_timeseries_resp = timeseries_ns.model('ReadTimeseriesResponse', {
    "has_prev": fields.Boolean,
    "has_next": fields.Boolean,
    "page": fields.Integer,
    "pages": fields.Integer(attribute="pages"),
    "items_per_page": fields.Integer(attribute="per_page"),
    "total_items": fields.Integer(attribute="total"),
    "items": fields.List(fields.Nested(timeseries)),
})
