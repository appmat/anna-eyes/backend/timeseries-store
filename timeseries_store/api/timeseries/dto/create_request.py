from typing import List
from flask_restx import fields
from flask_restx.reqparse import RequestParser

from timeseries_store.api.timeseries.endpoints import timeseries_ns

timeseries = timeseries_ns.model('TimeseriesRequest', {
    'name': fields.String,
    'points': fields.List(fields.Float),
})

create_timeseries_req = timeseries_ns.model('CreateTimeseriesRequest', {
    'timeseries': fields.List(fields.Nested(timeseries)),
})

def timeseries_name(name: str) -> str:
    """Validation method for name"""
    if len(name) == 0:
        raise ValueError(
            "timeseries name len must be more than 0"
        )
    return name


def timeseries_points(points: List[float]) -> List[float]:
    """Validation method for points"""
    if type(points).__name__ != "list":
        raise ValueError(
            "timeseries points must be list"
        )

    if len(points) == 0:
        raise ValueError(
            "timeseries points len must be more than 0"
        )

    if len(points) > 2048:
        raise ValueError(
            "timeseries points len must be less or equal than 2048"
        )

    for point in points:
        t = type(point).__name__
        if t != 'int' and t != 'float':
            raise ValueError(
                f"timeseries points elements must be int or float, but it {t}"
            )

    return points

def timeseries(t):
    if type(t).__name__ != "list":
        raise ValueError(
            "timeseries must be list"
        )

    if len(t) == 0:
        raise ValueError(
            "timeseries count must be more than 0"
        )

    if len(t) > 6:
        raise ValueError(
            "timeseries count must be less or equal than 6"
        )

    for e in t:
        timeseries_name(e.get('name'))
        timeseries_points(e.get('points'))

    return t

create_timeseries_reqparser = RequestParser(bundle_errors=True)
create_timeseries_reqparser.add_argument(
    "timeseries",
    type=timeseries,
    location="json",
    required=True,
    nullable=False,
)
