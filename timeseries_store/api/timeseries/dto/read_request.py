from email.policy import default
from flask_restx.inputs import positive
from flask_restx import fields
from flask_restx.reqparse import RequestParser

from timeseries_store.api.timeseries.endpoints import timeseries_ns

read_timeseries_req = timeseries_ns.model('ReadTimeseriesRequest', {
    'page': fields.Integer,
    'per_page': fields.Integer,
})

read_timeseries_reqparser = RequestParser(bundle_errors=True)
read_timeseries_reqparser.add_argument(
    "page",
    type=positive,
    default=1,
    location="json",
    required=False,
)
read_timeseries_reqparser.add_argument(
    "per_page",
    type=positive,
    location="json",
    required=False,
    choices=[5, 10, 25, 50, 100], 
    default=10,
)
