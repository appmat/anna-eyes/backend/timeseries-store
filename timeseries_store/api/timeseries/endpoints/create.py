from http import HTTPStatus
from flask_restx import Resource
from flask import jsonify

from timeseries_store.api.timeseries.dto.create_request import create_timeseries_reqparser, create_timeseries_req
from timeseries_store.api.timeseries.dto.create_response import create_timeseries_resp

from timeseries_store.api.timeseries.business.create_timeseries import create_timeseries

from timeseries_store.api.timeseries.endpoints import timeseries_ns

@timeseries_ns.route("/create", endpoint="timeseries_create")
@timeseries_ns.response(int(HTTPStatus.OK), "Retrieved timeseties.", create_timeseries_resp)
@timeseries_ns.response(int(HTTPStatus.BAD_REQUEST), "Validation error.")
@timeseries_ns.response(int(HTTPStatus.INTERNAL_SERVER_ERROR), "Internal server error.")
class TimeseriesCreate(Resource):
    """Handles HTTP requests to URL: /create."""

    # @timeseries_ns.doc(security="Bearer")
    # @timeseries_ns.response(int(HTTPStatus.FORBIDDEN), "Token is required.")
    # @timeseries_ns.response(int(HTTPStatus.OK), "Added new timeseries.")
    # timeseries_ns.expect(create_timeseries_reqparser)
    @timeseries_ns.doc('create_timeseries', description='Создать временные ряды', body=create_timeseries_req)
    @timeseries_ns.expect(create_timeseries_req, validate=True)
    def post(self):
        """Создать временные ряды"""
        timeseries_dict = create_timeseries_reqparser.parse_args()
        try:
            return create_timeseries(timeseries_dict)
        except Exception as e:
            response = jsonify(message=str(e))
            response.status_code = HTTPStatus.INTERNAL_SERVER_ERROR
            return response
