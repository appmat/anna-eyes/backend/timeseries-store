from flask_restx import Namespace

# from chat_api.api.messages.dto.pagination_model import pagination_model
# from chat_api.api.messages.dto.message_owner_model import message_owner_model
# from chat_api.api.messages.dto.message_model import message_model
# from chat_api.api.messages.dto.pagination_link_model import pagination_links_model

timeseries_ns = Namespace(name="timeseries", validate=True)
# timeseries_ns.models[message_owner_model.name] = message_owner_model
# timeseries_ns.models[message_model.name] = message_model
# timeseries_ns.models[pagination_links_model.name] = pagination_links_model
# timeseries_ns.models[pagination_model.name] = pagination_model

import timeseries_store.api.timeseries.endpoints.create
import timeseries_store.api.timeseries.endpoints.read
