from http import HTTPStatus
from flask_restx import Resource
from flask import jsonify

from timeseries_store.api.timeseries.dto.read_request import read_timeseries_reqparser, read_timeseries_req
from timeseries_store.api.timeseries.dto.read_response import read_timeseries_resp

from timeseries_store.api.timeseries.business.read_timeseries import read_timeseries

from timeseries_store.api.timeseries.endpoints import timeseries_ns


@timeseries_ns.route("/read", endpoint="timeseries_read")
@timeseries_ns.response(int(HTTPStatus.OK), "Retrieved timeseries.", read_timeseries_resp)
@timeseries_ns.response(int(HTTPStatus.BAD_REQUEST), "Validation error.")
@timeseries_ns.response(int(HTTPStatus.INTERNAL_SERVER_ERROR), "Internal server error.")
class TimeseriesRead(Resource):
    """Handles HTTP requests to URL: /read."""

    # @timeseries_ns.doc(security="Bearer")
    # @timeseries_ns.response(int(HTTPStatus.FORBIDDEN), "Token is required.")
    # @timeseries_ns.response(int(HTTPStatus.OK), "Added new timeseries.")
    # timeseries_ns.expect(create_timeseries_reqparser)
    @timeseries_ns.doc('read_timeseries', description='Считать временные ряды', body=read_timeseries_req)
    @timeseries_ns.expect(read_timeseries_req, validate=True)
    def post(self):
        """Считать временные ряд"""
        req_dict = read_timeseries_reqparser.parse_args()
        try:
            return read_timeseries(req_dict)
        except Exception as e:
            response = jsonify(message=str(e))
            response.status_code = HTTPStatus.INTERNAL_SERVER_ERROR
            return response
