"""Business logic for /widgets API endpoints."""
from http import HTTPStatus
import json
from typing import Dict

from flask import jsonify, url_for

from flask_restx import marshal

from timeseries_store import db
from timeseries_store.api.timeseries.dto.create_response import create_timeseries_resp
from timeseries_store.models.point import Point
from timeseries_store.models.timeseries import Timeseries

def create_timeseries(timeseries):
	response = {"result": []}
	for timeseries_dict in timeseries.get('timeseries'):
		timeseries = Timeseries()
		timeseries.name = timeseries_dict.get("name")
		points = []
		for index, point_value in enumerate(timeseries_dict.get("points")):
			p = Point()
			p.value = point_value
			p.index = index
			points.append(p)
		timeseries.points = points
		db.session.add(timeseries)
		response["result"].append(timeseries)
	db.session.commit()

	response_data = marshal(response, create_timeseries_resp)
	return jsonify(response_data)