"""Business logic for /widgets API endpoints."""
from flask import jsonify

from flask_restx import marshal

from timeseries_store.api.timeseries.dto.read_response import read_timeseries_resp
from timeseries_store.models.timeseries import Timeseries


def read_timeseries(req_dict):
	page = req_dict.get('page')
	per_page = req_dict.get('per_page')
	pagination = Timeseries.query.order_by(
            Timeseries.created_at.asc()
        ).paginate(page, per_page, error_out=False)

	response_data = marshal(pagination, read_timeseries_resp)
	response = jsonify(response_data)
	return response