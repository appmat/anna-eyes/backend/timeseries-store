from email.policy import default
from flask_restx.inputs import positive
from flask_restx import fields
from flask_restx.reqparse import RequestParser
import uuid
from timeseries_store.api.value.endpoints import value_ns

read_value_req = value_ns.model('ReadValueRequest', {
    'timeseries_ids': fields.List(fields.String),
})

def ids_valid(timeseries_ids):
    for index, t in enumerate(timeseries_ids):
        timeseries_ids[index] = int(t)
    return timeseries_ids

def uuid_valid(timeseries_id):
    try:
        uuid.UUID(timeseries_id)
        return timeseries_id
    except ValueError:
        raise ValueError(
            "is not uuid"
        )

read_value_reqparser = RequestParser(bundle_errors=True)
read_value_reqparser.add_argument(
    "timeseries_ids",
    type=ids_valid,
    location="json",
    required=True,
)
