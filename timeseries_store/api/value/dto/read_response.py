from typing import List
from attr import field
from flask_restx import Model, fields

from timeseries_store.api.value.endpoints import value_ns

value = value_ns.model('Value', {
    "id": fields.String,
    "timeseries_id": fields.String,
    "name": fields.String,
    "expr": fields.String,
    "value": fields.Float,
})

read_value_resp = value_ns.model('ReadValueResponse', {
    "values": fields.List(fields.Nested(value)),
})
