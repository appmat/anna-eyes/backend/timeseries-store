from email.policy import default
from flask_restx.inputs import positive
from flask_restx import fields
from flask_restx.reqparse import RequestParser
import uuid
from timeseries_store.api.value.endpoints import value_ns

value_to_create = value_ns.model('ValueToCreate', {
    'timeseries_id': fields.String,
    'name': fields.String,
    'expr': fields.String,
    'value': fields.Float,
})

create_value_req = value_ns.model('CreateValueRequest', {
    'values': fields.List(fields.Nested(value_to_create)),
})


def values_valid(values):
    for index, v in enumerate(values):
        values[index]["timeseties_id"] = int(v.get('timeseries_id'))

        if len(v.get('name')) == 0:
            raise ValueError(
                "name is empty"
            )

        if len(v.get('expr')) == 0:
            raise ValueError(
                "expr is empty"
            )

        if v.get('value') == 0:
            raise ValueError(
                "value is empty"
            )
    return values

create_value_reqparser = RequestParser(bundle_errors=True)
create_value_reqparser.add_argument(
    "values",
    type=values_valid,
    location="json",
    required=True,
)
