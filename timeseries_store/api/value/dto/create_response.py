from typing import List
from attr import field
from flask_restx import Model, fields

from timeseries_store.api.value.endpoints import value_ns
from timeseries_store.api.value.dto.read_response import value

create_value_resp = value_ns.model('CreateValueResponse', {
    "values": fields.List(fields.Nested(value)),
})
