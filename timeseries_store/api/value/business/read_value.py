"""Business logic for /widgets API endpoints."""
from flask import jsonify

from flask_restx import marshal

from timeseries_store.api.value.dto.read_response import read_value_resp
from timeseries_store.models.value import Value


def read_value(req_dict):
	timeseries_ids = req_dict.get('timeseries_ids')
	response = {
		"values": Value.query.
		order_by(
			Value.timeseries_id.asc(),
			Value.name.asc()
		).
		filter(Value.timeseries_id.in_(timeseries_ids)).all(),
	}

	response_data = marshal(response, read_value_resp)
	response = jsonify(response_data)
	return response