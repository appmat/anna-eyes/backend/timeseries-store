"""Business logic for /widgets API endpoints."""
from http import HTTPStatus
from typing import List
from flask import jsonify

from flask_restx import marshal

from timeseries_store import db
from timeseries_store.api.value.dto.create_response import create_value_resp
from timeseries_store.models.value import Value

def create_value(req_dict):
	values = []
	for v_dict in req_dict.get('values'):
		v = Value()
		v.timeseries_id = v_dict.get('timeseries_id')
		v.name = v_dict.get('name')
		v.expr = v_dict.get('expr')
		v.value = v_dict.get('value')
		values.append(v)
		db.session.add(v)
	db.session.commit()
	response = {
		"values": values,
	}
	response_data = marshal(response, create_value_resp)
	return jsonify(response_data)