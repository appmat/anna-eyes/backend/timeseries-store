from http import HTTPStatus
from flask_restx import Resource
from flask import jsonify

from timeseries_store.api.value.dto.read_request import read_value_reqparser, read_value_req
from timeseries_store.api.value.dto.read_response import read_value_resp

from timeseries_store.api.value.business.read_value import read_value

from timeseries_store.api.value.endpoints import value_ns


@value_ns.route("/read", endpoint="value_read")
@value_ns.response(int(HTTPStatus.OK), "Values", read_value_resp)
@value_ns.response(int(HTTPStatus.BAD_REQUEST), "Validation error.")
@value_ns.response(int(HTTPStatus.INTERNAL_SERVER_ERROR), "Internal server error.")
class ValueCreate(Resource):
    """Handles HTTP requests to URL: /read."""

    @value_ns.doc('value_read', description='Считать значение', body=read_value_req)
    @value_ns.expect(read_value_req, validate=True)
    def post(self):
        """Считать значение"""
        req_dict = read_value_reqparser.parse_args()
        try:
            return read_value(req_dict)
        except Exception as e:
            response = jsonify(message=str(e))
            response.status_code = HTTPStatus.INTERNAL_SERVER_ERROR
            return response
