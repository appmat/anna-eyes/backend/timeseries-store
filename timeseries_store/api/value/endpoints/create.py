from http import HTTPStatus
from flask_restx import Resource
from flask import jsonify

from timeseries_store.api.value.dto.create_request import create_value_reqparser, create_value_req
from timeseries_store.api.value.dto.create_response import create_value_resp

from timeseries_store.api.value.business.create_value import create_value

from timeseries_store.api.value.endpoints import value_ns


@value_ns.route("/create", endpoint="value_create")
@value_ns.response(int(HTTPStatus.OK), "Created value", create_value_resp)
@value_ns.response(int(HTTPStatus.BAD_REQUEST), "Validation error.")
@value_ns.response(int(HTTPStatus.INTERNAL_SERVER_ERROR), "Internal server error.")
class ValueCreate(Resource):
    """Handles HTTP requests to URL: /create."""

    @value_ns.doc('value_create', description='Создать значение', body=create_value_req)
    @value_ns.expect(create_value_req, validate=True)
    def post(self):
        """Создать значение"""
        req_dict = create_value_reqparser.parse_args()
        try:
            return create_value(req_dict)
        except Exception as e:
            response = jsonify(message=str(e))
            response.status_code = HTTPStatus.INTERNAL_SERVER_ERROR
            return response
