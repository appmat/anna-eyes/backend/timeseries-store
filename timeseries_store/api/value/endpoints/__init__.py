from flask_restx import Namespace

value_ns = Namespace(name="value", validate=True)

import timeseries_store.api.value.endpoints.create
import timeseries_store.api.value.endpoints.read
