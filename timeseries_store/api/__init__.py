"""API blueprint configuration."""
from flask import Blueprint
from flask_restx import Api

from timeseries_store.api.timeseries.endpoints import timeseries_ns
from timeseries_store.api.point.endpoints import point_ns
from timeseries_store.api.value.endpoints import value_ns

api_bp = Blueprint("api", __name__, url_prefix="/api/v1")
authorizations = {"Bearer": {
    "type": "apiKey",
    "in": "header",
    "name": "Authorization",
    "description": "Введите в *'Value'* рассположенный ниже: **'Bearer &lt;JWT&gt;'**, где JWT это токен",
}}

api = Api(
    api_bp,
    version="1.0",
    title="Timeseries Store API",
    description="Сервис для хранения временных рядов, точек и значений",
    doc="/docs",
    authorizations=authorizations,
)

api.add_namespace(timeseries_ns, path="/timeseries")
api.add_namespace(point_ns, path="/point")
api.add_namespace(value_ns, path="/value")
# api.add_namespace(messages_ns, path="/messages")
