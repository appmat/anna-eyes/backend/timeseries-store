from http import HTTPStatus
from flask_restx import Resource
from flask import jsonify

from timeseries_store.api.point.dto.read_request import read_point_reqparser, read_point_req
from timeseries_store.api.point.dto.read_response import read_point_resp

from timeseries_store.api.point.business.read_point import read_point

from timeseries_store.api.point.endpoints import point_ns


@point_ns.route("/read", endpoint="point_read")
@point_ns.response(int(HTTPStatus.OK), "Retrieved points.", read_point_resp)
@point_ns.response(int(HTTPStatus.BAD_REQUEST), "Validation error.")
@point_ns.response(int(HTTPStatus.INTERNAL_SERVER_ERROR), "Internal server error.")
class TimeseriesRead(Resource):
    """Handles HTTP requests to URL: /read."""

    @point_ns.doc('point_read', description='Получить точки временных рядов', body=read_point_req)
    @point_ns.expect(read_point_req, validate=True)
    def post(self):
        """Получить точки временных рядов"""
        req_dict = read_point_reqparser.parse_args()
        try:
            return read_point(req_dict)
        except Exception as e:
            response = jsonify(message=str(e))
            response.status_code = HTTPStatus.INTERNAL_SERVER_ERROR
            return response
