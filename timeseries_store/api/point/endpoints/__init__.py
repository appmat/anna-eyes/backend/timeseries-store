from flask_restx import Namespace

point_ns = Namespace(name="point", validate=True)

import timeseries_store.api.point.endpoints.read
import timeseries_store.api.point.endpoints.mark
