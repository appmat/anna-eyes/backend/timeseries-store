from http import HTTPStatus
from flask_restx import Resource
from flask import jsonify

from timeseries_store.api.point.dto.mark_request import mark_point_reqparser, mark_point_req
from timeseries_store.api.point.dto.mark_response import mark_point_resp

from timeseries_store.api.point.business.mark_point import mark_point

from timeseries_store.api.point.endpoints import point_ns


@point_ns.route("/mark", endpoint="point_mark")
@point_ns.response(int(HTTPStatus.OK), "Marked Point", mark_point_resp)
@point_ns.response(int(HTTPStatus.BAD_REQUEST), "Validation error.")
@point_ns.response(int(HTTPStatus.INTERNAL_SERVER_ERROR), "Internal server error.")
class TimeseriesRead(Resource):
    """Handles HTTP requests to URL: /mark."""

    @point_ns.doc('point_mark', description='Отметить точки на временных рядах', body=mark_point_req)
    @point_ns.expect(mark_point_req, validate=True)
    def post(self):
        """Отметить точки на временных рядах"""
        req_dict = mark_point_reqparser.parse_args()
        try:
            return mark_point(req_dict)
        except Exception as e:
            response = jsonify(message=str(e))
            response.status_code = HTTPStatus.INTERNAL_SERVER_ERROR
            return response
