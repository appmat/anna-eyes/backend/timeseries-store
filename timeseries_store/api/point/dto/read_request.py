from email.policy import default
from flask_restx.inputs import positive
from flask_restx import fields
from flask_restx.reqparse import RequestParser
import uuid
from timeseries_store.api.point.endpoints import point_ns

read_point_req = point_ns.model('ReadPointRequest', {
    'timeseries_ids': fields.List(fields.String),
})

def ids_valid(ids):
    new_ids = []
    for id in ids:
        new_ids.append(int(id))
    return new_ids


read_point_reqparser = RequestParser(bundle_errors=True)
read_point_reqparser.add_argument(
    "timeseries_ids",
    type=ids_valid,
    location="json",
    required=True,
)
