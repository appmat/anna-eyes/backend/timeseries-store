from typing import List
from attr import field
from flask_restx import Model, fields

from timeseries_store.api.point.endpoints import point_ns

point = point_ns.model('Point', {
    "id": fields.String,
    "timeseries_id": fields.String,
    "name": fields.String,
    "marked": fields.Boolean,
    "value": fields.Float,
    "index": fields.Integer,
})

read_point_resp = point_ns.model('ReadPointResponse', {
    "points": fields.List(fields.Nested(point)),
})
