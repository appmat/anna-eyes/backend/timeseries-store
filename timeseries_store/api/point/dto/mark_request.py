from flask_restx import fields
from flask_restx.reqparse import RequestParser
import uuid
from timeseries_store.api.point.endpoints import point_ns

point_to_mark = point_ns.model('PointToMark', {
    'id': fields.String,
    'name': fields.String,
})

mark_point_req = point_ns.model('MarkPointRequest', {
    'points': fields.List(fields.Nested(point_to_mark)),
})

def points(points):
    if len(points) > 16:
        raise ValueError(
            "list of points to mark must be leass or equal than 16"
        )

    for index, p in enumerate(points):
        points[index]["id"] = int(p.get("id"))

        if len(p.get('name')) == 0:
            raise ValueError(
                "point name must be not empty"
            )
    print(points)
    return points

mark_point_reqparser = RequestParser(bundle_errors=True)
mark_point_reqparser.add_argument(
    "points",
    type=points,
    location="json",
    required=True,
)
