"""Business logic for /widgets API endpoints."""
from http import HTTPStatus
from typing import List
from flask import jsonify

from flask_restx import marshal

from timeseries_store import db
from timeseries_store.api.point.dto.read_response import read_point_resp
from timeseries_store.models.point import Point
from timeseries_store.models.value import Value

def mark_point(req_dict):
	points_map = {}
	points_id = []
	for p in req_dict.get('points'):
		points_id.append(p.get('id'))
		points_map[p.get('id')] = p

	points: List[Point] = Point.query.filter(Point.id.in_(points_id)).all()

	for p in points:
		name = points_map[p.id].get('name')
		if p.marked and p.name != name:
			raise Exception(
                "point already marked with other name"
            )

		if not p.marked:
			p.marked = True
			p.name = name
	try:
		db.session.commit()
	except Exception as e:
		response = jsonify(error=str(e))
		response.status_code = HTTPStatus.INTERNAL_SERVER_ERROR
		return response

	response = jsonify(success=True)
	return response