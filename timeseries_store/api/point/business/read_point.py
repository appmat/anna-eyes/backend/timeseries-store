"""Business logic for /widgets API endpoints."""
from flask import jsonify

from flask_restx import marshal

from timeseries_store.api.point.dto.read_response import read_point_resp
from timeseries_store.models.point import Point


def read_point(req_dict):
	timeseries_ids = req_dict.get('timeseries_ids')
	response = {
		"points": Point.query.
		order_by(
			Point.timeseries_id.asc(),
			Point.index.asc()
		).
		filter(Point.timeseries_id.in_(timeseries_ids)).all(),
	}

	response_data = marshal(response, read_point_resp)
	response = jsonify(response_data)
	return response