"""Installation script for timeseries-store application."""
from pathlib import Path
from setuptools import setup, find_packages

DESCRIPTION = (
    "Timeseries Store",
)
APP_ROOT = Path(__file__).parent
README = (APP_ROOT / "README.md").read_text()
AUTHOR = "Dinaf Rakipov"
AUTHOR_EMAIL = "rakipov69@gmail.com"
PROJECT_URLS = {
    "Documentation": "TODO",
    "Bug Tracker": "TODO",
    "Source Code": "TODO",
}
INSTALL_REQUIRES = [
    "Flask",
    "Flask-Bcrypt",
    "Flask-Cors",
    "Flask-Migrate",
    "flask-restx",
    "Flask-SQLAlchemy",
    "PyJWT",
    "python-dateutil",
    "python-dotenv",
    "requests",
    "urllib3",
    "werkzeug",
    "psycopg2",
    "flask_healthz",
    "prometheus_flask_exporter",
]
EXTRAS_REQUIRE = {
    "dev": [
        "black",
        "flake8",
        "pre-commit",
        "pydocstyle",
        "pytest",
        "pytest-black",
        "pytest-clarity",
        "pytest-dotenv",
        "pytest-flake8",
        "pytest-flask",
        "tox",
    ]
}

setup(
    name="timeseries-store",
    description=DESCRIPTION,
    long_description=README,
    long_description_content_type="text/markdown",
    version="1.0.0",
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    maintainer=AUTHOR,
    maintainer_email=AUTHOR_EMAIL,
    license="MIT",
    url="TODO",
    project_urls=PROJECT_URLS,
    packages=find_packages(where="."),
    package_dir={"": "."},
    python_requires=">=3.8",
    install_requires=INSTALL_REQUIRES,
    extras_require=EXTRAS_REQUIRE,
)
