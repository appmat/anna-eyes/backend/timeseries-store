"""Flask CLI/Application entry point."""
import os

from timeseries_store import create_app, db
from timeseries_store.models.timeseries import Timeseries
from timeseries_store.models.point import Point
from timeseries_store.models.value import Value

app = create_app(os.getenv("FLASK_ENV", "development"))

@app.shell_context_processor
def shell():
    return {
        "db": db,
        "Timeseries": Timeseries,
        "Point": Point,
        "Value": Value,
    }
